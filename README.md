## Information

Hello, I will be using https://www.shiplowcost.com/ to ship some things and I couldn't find a "paste all your inventory" in one place,  
so I made a quick script that reads .csv ( comma separated values ) file and generates an output with the exact price of the shipment in total and per item.

You can also use an Excel file or Google Sheet and just export to CSV following the format:  

```
,,,,
Height,Length,Width,Weight,Name
,,,,
77,57,66,25,Big Box 0
77,57,66,25,Big Box 1
77,57,66,25,Big Box 2
77,57,66,25,Big Box 3
77,57,66,25,Big Box 4
77,57,66,25,Big Box 5
77,57,66,25,Big Box 6
77,57,66,25,Big Box 7
60,45,45,23.2,Cutie1
60,45,45,21.5,Cutie2
```

## Usage

The script needs the following python packages:

```
import requests
import json
import csv
from decimal import Decimal
```

from which you only need to install `requests` with `pip install requests` or `pip3 install requests`.
  

After making sure we have all the dependencies and CSV file we can now run: 
  
`python3 shipment.py`

That will generate something around this:

```
Item Big Box 0 costs €57.50
 - - - - - 
Item Big Box 1 costs €57.50
 - - - - - 
Item Big Box 2 costs €57.50
 - - - - - 
Item Big Box 3 costs €57.50
 - - - - - 
Item Big Box 4 costs €57.50
 - - - - - 
Item Big Box 5 costs €57.50
 - - - - - 
Item Big Box 6 costs €57.50
 - - - - - 
Item Big Box 7 costs €57.50
 - - - - - 
Item Cutie1 costs €57.50
 - - - - - 
Item Cutie2 costs €57.50
 - - - - - 
Total cost of shipment: 575.00 €
```
  

I hope you found this useful!