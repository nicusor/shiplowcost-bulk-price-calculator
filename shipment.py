import requests
import json
import csv
from decimal import Decimal

total = 0

### Variables

fromCountryId = "6"
toCountryId = "132"

with open('moving.csv', newline='') as csvfile:
     spamreader = csv.reader(csvfile)
     for row in spamreader:

        height = row[0]
        length = row[1]
        width = row[2]
        weight = row[3]
        name = row[4]

        if not name or name == "Name":
            continue

        response = requests.post('https://www.shiplowcost.com/Services/CalculatorService.asmx/CalculatePrice', json={'fromCountryId': fromCountryId, 'toCountryId': toCountryId, 'postCode': "", 'length': length, 'width': width, 'height': height, 'weight': weight, 'quantity': "1", 'isParcelDocument': "false", 'unknownDimensions': "false"})

        responseObject = json.loads(response.text)
        pricePlusTva = responseObject["d"]["Parcel"]["PriceFormatted"].split("€")[1]
        total += Decimal(pricePlusTva)

        print("Item " + name + " costs €" + pricePlusTva)
        print(" - - - - - ")


print("Total cost of shipment: " + str(total) + " €")